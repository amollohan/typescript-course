function startGame() {
    // starting a new game

    let playerName: string = 'Andrew';
    logPlayer(playerName);

    var messageElemenent = document.getElementById('messages');
    messageElemenent!.innerText = 'Begin Game';
}

function logPlayer(name: string) {
    console.log(`New game starting for ${name}`)
}
document.getElementById('startGame')!.addEventListener('click', startGame)